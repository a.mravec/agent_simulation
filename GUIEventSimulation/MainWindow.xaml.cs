﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using OSPABA;
using simulation;

namespace GUIEventSimulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public SeriesCollection SeriesCollection { get; set; }
        public SeriesCollection SeriesCollectionForExperiments { get; set; }
        public LineSeries LineSeriesAverageTimeInSystemLend { get; set; }
        public LineSeries LineSeriesAverageTimeInSystemReturn { get; set; }

        public List<MySimulation> Simulations { get; set; } = new List<MySimulation>();
        public double OneStep { get; set; }
        public MySimulation Simulation { get; set; }
        public ObservableCollection<Core.entities.Group> TerminalFirstObservableCollection { get; set; } = new ObservableCollection<Core.entities.Group>();
        public ObservableCollection<Core.entities.Group> TerminalSecondObservableCollection { get; set; } = new ObservableCollection<Core.entities.Group>();
        public ObservableCollection<Core.entities.Group> BaseObservableCollection { get; set; } = new ObservableCollection<Core.entities.Group>();
        public ObservableCollection<Core.entities.Group> RentalOutObservableCollection { get; set; } = new ObservableCollection<Core.entities.Group>();
        public bool IsStoped { get; set; } = false;
        public bool IsTurbo { get; set; } = false;
        public bool IsStarted { get; set; } = false;
        public bool SetResults { get; set; } = false;
        public double SimulateTime { get; set; } = 4.5 * 60 * 60 + 1000000;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            InitReplication();
            InitSimulation();
            RadioButtonFixedBus.IsChecked = true;
            TextBoxToNumberOfBusSimulation.IsEnabled = false;

            LineSeriesAverageTimeInSystemLend = new LineSeries()
            {
                Title = "Average time in system to lend car [min]",
                Fill = Brushes.Transparent,
                PointGeometrySize = 5,
            };

            LineSeriesAverageTimeInSystemReturn = new LineSeries()
            {
                Title = "Average time in system to return car [min]",
                Fill = Brushes.Transparent,
                PointGeometrySize = 5,
            };

            SeriesCollection = new SeriesCollection()
            {
                LineSeriesAverageTimeInSystemLend,
                LineSeriesAverageTimeInSystemReturn
            };

            SeriesCollectionForExperiments = new SeriesCollection();
        }

        private void InitReplication()
        {
            ButtonStartReplications.IsEnabled = true;
            ButtonPauseReplications.IsEnabled = false;
            ButtonContinueReplications.IsEnabled = false;
            ButtonStopReplications.IsEnabled = false;

            TextBoxNumberOfBusReplications.IsEnabled = true;
            TextBoxNumberOfService.IsEnabled = true;
        }

        private string GetNiceTime(double time, string format = @"hh\:mm\:ss")
        {
            if (Double.IsNaN(time) || Double.IsInfinity(time))
            {
                return 0.0.ToString(format);
            }
            TimeSpan timeSpan = TimeSpan.FromSeconds(time);
            return timeSpan.ToString(format);
        }

        private void RegisterEvents()
        {
            // time
            Simulation.SetCurrentTimeEvent += (time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    LabelSimulationTime.Content = GetNiceTime(time, @"dd\ hh\:mm\:ss");
                });
            };

            // money
            Simulation.UpdateMoney += (month, simulation) =>
            {
                Dispatcher.Invoke(() =>
                {
                    LabelMonthMoney.Content = month.ToString("F") + " €";
                    LabelSimMoney.Content = simulation.ToString("F") + " €";
                });
            };

            // logs
            Simulation.TestEvent += (log) =>
            {
                Dispatcher.Invoke(() =>
                {
                    ListViewLog.Items.Add(log);
                });
            };

            // update terminal first queue
            Simulation.UpdateGroupToTerminalFirstQueueEvent += (group, add, statLength, statTime) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if (add)
                    {
                        if (group != null)
                        {
                            TerminalFirstObservableCollection.Add(group);
                        }

                        LabelLengthTerminalFirst.Content = TerminalFirstObservableCollection.Count.ToString();
                        LabelMaxLengthTerminalFirst.Content = Math.Round(statLength.MaxValue, 0).ToString();
                        LabelAverageLengthTerminalFirst.Content = statLength.GetAverage().ToString("F");
                        LabelIntervalLengthTerminalFirst.Content = "<" + statLength.GetLeftInterval90().ToString("F") + " , " + statLength.GetRightInterval90().ToString("F") + ">";
                    }
                    else
                    {
                        if (group != null)
                        {
                            TerminalFirstObservableCollection.Remove(group);
                        }
                        LabelLengthTerminalFirst.Content = TerminalFirstObservableCollection.Count.ToString();
                        LabelMaxLengthTerminalFirst.Content = Math.Round(statLength.MaxValue, 0).ToString();
                        LabelAverageLengthTerminalFirst.Content = statLength.GetAverage().ToString("F");
                        LabelIntervalLengthTerminalFirst.Content = "<" + statLength.GetLeftInterval90().ToString("F") + " , " + statLength.GetRightInterval90().ToString("F") + ">";
                        LabelMaxTimeTerminalFirst.Content = GetNiceTime(statTime.MaxValue);
                        LabelAverageTimeTerminalFirst.Content = GetNiceTime(statTime.GetAverage());
                        LabelIntervalTimeTerminalFirst.Content = "<" + GetNiceTime(statTime.GetLeftInterval90()) + " , " + GetNiceTime(statTime.GetRightInterval90()) + ">";
                    }
                });
            };

            // update terminal second queue
            Simulation.UpdateGroupToTerminalSecondQueueEvent += (group, add, statLength, statTime) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if (add)
                    {
                        if (group != null)
                        {
                            TerminalSecondObservableCollection.Add(group);
                        }

                        LabelLengthTerminalSecond.Content = TerminalSecondObservableCollection.Count.ToString();
                        LabelMaxLengthTerminalSecond.Content = Math.Round(statLength.MaxValue, 0).ToString();
                        LabelAverageLengthTerminalSecond.Content = statLength.GetAverage().ToString("F");
                        LabelIntervalLengthTerminalSecond.Content = "<" + statLength.GetLeftInterval90().ToString("F") + " , " + statLength.GetRightInterval90().ToString("F") + ">";
                    }
                    else
                    {
                        if (group != null)
                        {
                            TerminalSecondObservableCollection.Remove(group);
                        }
                        LabelLengthTerminalSecond.Content = TerminalSecondObservableCollection.Count.ToString();
                        LabelMaxLengthTerminalSecond.Content = Math.Round(statLength.MaxValue, 0).ToString();
                        LabelAverageLengthTerminalSecond.Content = statLength.GetAverage().ToString("F");
                        LabelIntervalLengthTerminalSecond.Content = "<" + statLength.GetLeftInterval90().ToString("F") + " , " + statLength.GetRightInterval90().ToString("F") + ">";
                        LabelMaxTimeTerminalSecond.Content = GetNiceTime(statTime.MaxValue);
                        LabelAverageTimeTerminalSecond.Content = GetNiceTime(statTime.GetAverage());
                        LabelIntervalTimeTerminalSecond.Content = "<" + GetNiceTime(statTime.GetLeftInterval90()) + " , " + GetNiceTime(statTime.GetRightInterval90()) + ">";
                    }
                });
            };

            // add person to rental in queue
            Simulation.UpdateGroupToRentalInQueueEvent += (group, add, statLength, statTime) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if (add)
                    {
                        if (group != null)
                        {
                            BaseObservableCollection.Add(group);
                        }

                        LabelLengthBase.Content = BaseObservableCollection.Count.ToString();
                        LabelMaxLengthBase.Content = Math.Round(statLength.MaxValue, 0).ToString();
                        LabelAverageLengthBase.Content = statLength.GetAverage().ToString("F");
                        LabelIntervalLengthBase.Content = "<" + statLength.GetLeftInterval90().ToString("F") + " , " + statLength.GetRightInterval90().ToString("F") + ">";
                    }
                    else
                    {
                        if (group != null)
                        {
                            BaseObservableCollection.Remove(group);
                        }
                        LabelLengthBase.Content = BaseObservableCollection.Count.ToString();
                        LabelMaxLengthBase.Content = Math.Round(statLength.MaxValue, 0).ToString();
                        LabelAverageLengthBase.Content = statLength.GetAverage().ToString("F");
                        LabelIntervalLengthBase.Content = "<" + statLength.GetLeftInterval90().ToString("F") + " , " + statLength.GetRightInterval90().ToString("F") + ">";
                        LabelMaxTimeBase.Content = GetNiceTime(statTime.MaxValue);
                        LabelAverageTimeBase.Content = GetNiceTime(statTime.GetAverage());
                        LabelIntervalTimeBase.Content = "<" + GetNiceTime(statTime.GetLeftInterval90()) + " , " + GetNiceTime(statTime.GetRightInterval90()) + ">";
                    }
                });
            };

            // add person to rental out queue
            Simulation.UpdateGroupToRentalOutQueueEvent += (group, add, statLength, statTime) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if (add)
                    {
                        if (group != null)
                        {
                            RentalOutObservableCollection.Add(group);
                        }

                        LabelLengthRentalOut.Content = RentalOutObservableCollection.Count.ToString();
                        LabelMaxLengthRentalOut.Content = Math.Round(statLength.MaxValue, 0).ToString();
                        LabelAverageLengthRentalOut.Content = statLength.GetAverage().ToString("F");
                        LabelIntervalLengthRentalOut.Content = "<" + statLength.GetLeftInterval90().ToString("F") + " , " + statLength.GetRightInterval90().ToString("F") + ">";
                    }
                    else
                    {
                        if (group != null)
                        {
                            RentalOutObservableCollection.Remove(group);
                        }
                        LabelLengthRentalOut.Content = RentalOutObservableCollection.Count.ToString();
                        LabelMaxLengthRentalOut.Content = Math.Round(statLength.MaxValue, 0).ToString();
                        LabelAverageLengthRentalOut.Content = statLength.GetAverage().ToString("F");
                        LabelIntervalLengthRentalOut.Content = "<" + statLength.GetLeftInterval90().ToString("F") + " , " + statLength.GetRightInterval90().ToString("F") + ">";
                        LabelMaxTimeRentalOut.Content = GetNiceTime(statTime.MaxValue);
                        LabelAverageTimeRentalOut.Content = GetNiceTime(statTime.GetAverage());
                        LabelIntervalTimeRentalOut.Content = "<" + GetNiceTime(statTime.GetLeftInterval90()) + " , " + GetNiceTime(statTime.GetRightInterval90()) + ">";
                    }
                });
            };

            // person leave system
            Simulation.UpdateTimeInSystemEvent += (lendCar, returnCar, time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    // lend car
                    double minLend = (Math.Abs(lendCar.MinValue - double.MaxValue) <= 0) ? 0 : lendCar.MinValue;
                    LabelMinTimeInSystem.Content = GetNiceTime(minLend);
                    LabelMaxTimeInSystem.Content = GetNiceTime(lendCar.MaxValue);
                    LabelAverageTimeInSystem.Content = GetNiceTime(lendCar.GetAverage());
                    LabelInterval90.Content = "<" + GetNiceTime(lendCar.GetLeftInterval90()) + " , " + GetNiceTime(lendCar.GetRightInterval90()) + ">";
                    LabelInterval95.Content = "<" + GetNiceTime(lendCar.GetLeftInterval95()) + " , " + GetNiceTime(lendCar.GetRightInterval95()) + ">";

                    // return car
                    double minReturn = (Math.Abs(returnCar.MinValue - double.MaxValue) <= 0) ? 0 : returnCar.MinValue;
                    LabelMinTimeInSystemReturn.Content = GetNiceTime(minReturn);
                    LabelMaxTimeInSystemReturn.Content = GetNiceTime(returnCar.MaxValue);
                    LabelAverageTimeInSystemReturn.Content = GetNiceTime(returnCar.GetAverage());
                    LabelInterval90Return.Content = "<" + GetNiceTime(returnCar.GetLeftInterval90()) + " , " + GetNiceTime(returnCar.GetRightInterval90()) + ">";
                    LabelInterval95Return.Content = "<" + GetNiceTime(returnCar.GetLeftInterval95()) + " , " + GetNiceTime(returnCar.GetRightInterval95()) + ">";

                    // graph
                    LineSeriesAverageTimeInSystemLend.Values.Add(new ObservablePoint(time, lendCar.GetAverage() / 60));
                    LineSeriesAverageTimeInSystemReturn.Values.Add(new ObservablePoint(time, returnCar.GetAverage() / 60));
                });
            };

            // bus statistics
            Simulation.UpdateBusStatistics += (statAverage, statMax) =>
            {
                Dispatcher.Invoke(() =>
                {
                    LabelMaxUtilizationBus.Content = statMax.MaxValue.ToString("F");
                    LabelAverageUtilizationBus.Content = statAverage.GetAverage().ToString("F");
                    LabelIntervalBus.Content = "<" + statAverage.GetLeftInterval90().ToString("F") + " , " + statAverage.GetRightInterval90().ToString("F") + ">";
                });
            };

            // service statistics
            Simulation.UpdateServiceStatistics += (stat) =>
            {
                Dispatcher.Invoke(() =>
                {
                    LabelMaxUtilizationService.Content = stat.MaxValue;
                    LabelAverageUtilizationService.Content = stat.GetAverage().ToString("F");
                    LabelIntervalService.Content = "<" + stat.GetLeftInterval90().ToString("F") + " , " + stat.GetRightInterval90().ToString("F") + ">";
                });
            };
        }

        private async void ButtonStartReplication_OnClick(object sender, RoutedEventArgs e)
        {
            SetResults = false;
            IsStarted = true;
            IsStoped = false;
            ResetReplication();
            Simulation = new MySimulation();
            Simulation.NumberOfBus = int.Parse(TextBoxNumberOfBusReplications.Text);
            Simulation.NumberOfService = int.Parse(TextBoxNumberOfService.Text);
            ComboBoxItem item = (ComboBoxItem) ComboBoxBusType.SelectedItem;
            Simulation.SetBusType(item.Tag.ToString());
            SetSpeedSimulation();

            RegisterEvents();

            ButtonStartReplications.IsEnabled = false;
            ButtonPauseReplications.IsEnabled = true;
            ButtonContinueReplications.IsEnabled = false;
            ButtonStopReplications.IsEnabled = true;
            TextBoxNumberOfBusReplications.IsEnabled = false;
            TextBoxNumberOfService.IsEnabled = false;

            await Task.Run(() => { Simulation.Simulate(1, SimulateTime); });
        }

        private void SetSpeedSimulation()
        {
            if (IsTurbo)
            {
                //Simulation.SetMaxSimSpeed();
                Simulation.SetSimSpeed(1, 0);
            }
            else
            {
                double time = (10 - SliderSpeed.Value) / (10 + 10 - SliderSpeed.Value);
                Simulation.SetSimSpeed(1, time);
            }
        }

        private void ButtonPauseReplication_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonStartReplications.IsEnabled = false;
            ButtonPauseReplications.IsEnabled = false;
            ButtonContinueReplications.IsEnabled = true;
            ButtonStopReplications.IsEnabled = true;

            Simulation.PauseSimulation();
        }

        private void ButtonStopReplication_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonStartReplications.IsEnabled = true;
            ButtonPauseReplications.IsEnabled = false;
            ButtonContinueReplications.IsEnabled = false;
            ButtonStopReplications.IsEnabled = false;
            TextBoxNumberOfBusReplications.IsEnabled = true;
            TextBoxNumberOfService.IsEnabled = true;
            ListViewLog.Items.Clear();

            Simulation.StopSimulation();
            IsStarted = false;
            IsStoped = true;
            ResetReplication();
        }

        private void ResetReplication()
        {
            LineSeriesAverageTimeInSystemLend.Values = new ChartValues<ObservablePoint>();
            LineSeriesAverageTimeInSystemReturn.Values = new ChartValues<ObservablePoint>();

            TerminalFirstObservableCollection.Clear();
            TerminalSecondObservableCollection.Clear();
            BaseObservableCollection.Clear();
            RentalOutObservableCollection.Clear();

            LabelSimulationTime.Content = "00 00:00:00";

            LabelLengthTerminalFirst.Content = "0";
            LabelMaxLengthTerminalFirst.Content = "0";
            LabelAverageLengthTerminalFirst.Content = "0";
            LabelIntervalLengthTerminalFirst.Content = "< , >";
            LabelMaxTimeTerminalFirst.Content = "0";
            LabelAverageTimeTerminalFirst.Content = "0";
            LabelIntervalTimeTerminalFirst.Content = "< , >";

            LabelLengthTerminalSecond.Content = "0";
            LabelMaxLengthTerminalSecond.Content = "0";
            LabelAverageLengthTerminalSecond.Content = "0";
            LabelIntervalLengthTerminalSecond.Content = "< , >";
            LabelMaxTimeTerminalSecond.Content = "0";
            LabelAverageTimeTerminalSecond.Content = "0";
            LabelIntervalTimeTerminalSecond.Content = "< , >";

            LabelLengthBase.Content = "0";
            LabelMaxLengthBase.Content = "0";
            LabelAverageLengthBase.Content = "0";
            LabelIntervalLengthBase.Content = "< , >";
            LabelMaxTimeBase.Content = "0";
            LabelAverageTimeBase.Content = "0";
            LabelIntervalTimeBase.Content = "< , >";

            LabelLengthRentalOut.Content = "0";
            LabelMaxLengthRentalOut.Content = "0";
            LabelAverageLengthRentalOut.Content = "0";
            LabelIntervalLengthRentalOut.Content = "< , >";
            LabelMaxTimeRentalOut.Content = "0";
            LabelAverageTimeRentalOut.Content = "0";
            LabelIntervalTimeRentalOut.Content = "< , >";

            LabelMinTimeInSystem.Content = "0";
            LabelMaxTimeInSystem.Content = "0";
            LabelAverageTimeInSystem.Content = "0";
            LabelInterval90.Content = "< , >";
            LabelInterval95.Content = "< , >";

            LabelMinTimeInSystemReturn.Content = "0";
            LabelMaxTimeInSystemReturn.Content = "0";
            LabelAverageTimeInSystemReturn.Content = "0";
            LabelInterval90Return.Content = "< , >";
            LabelInterval95Return.Content = "< , >";

            LabelMaxUtilizationBus.Content = "0";
            LabelAverageUtilizationBus.Content = "0";
            LabelIntervalBus.Content = "< , >";

            LabelMaxUtilizationService.Content = "0";
            LabelAverageUtilizationService.Content = "0";
            LabelIntervalService.Content = "< , >";

            LabelMonthMoney.Content = "0 €";
            LabelSimMoney.Content = "0 €";
        }

        private void ButtonContinueReplication_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonStartReplications.IsEnabled = false;
            ButtonPauseReplications.IsEnabled = true;
            ButtonContinueReplications.IsEnabled = false;
            ButtonStopReplications.IsEnabled = true;

            Simulation.ResumeSimulation();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void CheckBoxTurbo_OnClick(object sender, RoutedEventArgs e)
        {
            if (CheckBoxTurbo.IsChecked != null) IsTurbo = (bool)CheckBoxTurbo.IsChecked;
            if (Simulation != null)
            {
                SetSpeedSimulation();
            }
        }

        private void SliderSpeed_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Simulation != null)
            {
                SetSpeedSimulation();
            }
        }

        //############################################################################################################

        private void InitSimulation()
        {
            ButtonStartSimulation.IsEnabled = true;
            ButtonPauseSimulation.IsEnabled = false;
            ButtonContinueSimulation.IsEnabled = false;
            ButtonStopSimulation.IsEnabled = false;

            TextBoxFromNumberOfServiceSimulation.IsEnabled = true;
            TextBoxFromNumberOfBusSimulation.IsEnabled = true;
            if (RadioButtonFixedService.IsChecked.Value)
            {
                TextBoxToNumberOfBusSimulation.IsEnabled = true;
            }
            else
            {
                TextBoxToNumberOfServiceSimulation.IsEnabled = true;
            }
            TextBoxNumberOfReplicationsSimulation.IsEnabled = true;

            RadioButtonFixedBus.IsEnabled = true;
            RadioButtonFixedService.IsEnabled = true;
        }

        private void ButtonStartSimulation_OnClick(object sender, RoutedEventArgs e)
        {
            ResetSimulation();
            Simulations.Clear();
            SeriesCollectionForExperiments.Clear();

            ButtonStartSimulation.IsEnabled = false;
            ButtonContinueSimulation.IsEnabled = false;
            ButtonPauseSimulation.IsEnabled = true;
            ButtonStopSimulation.IsEnabled = false;

            TextBoxFromNumberOfServiceSimulation.IsEnabled = false;
            TextBoxFromNumberOfBusSimulation.IsEnabled = false;
            TextBoxToNumberOfBusSimulation.IsEnabled = false;
            TextBoxToNumberOfServiceSimulation.IsEnabled = false;
            TextBoxNumberOfReplicationsSimulation.IsEnabled = false;

            RadioButtonFixedBus.IsEnabled = false;
            RadioButtonFixedService.IsEnabled = false;

            Experiment();
        }

        private async void Experiment()
        {
            Window.Cursor = Cursors.AppStarting;
            var watch = System.Diagnostics.Stopwatch.StartNew();

            int count = RadioButtonFixedService.IsChecked.Value ?
                int.Parse(TextBoxToNumberOfBusSimulation.Text) - int.Parse(TextBoxFromNumberOfBusSimulation.Text)
                : int.Parse(TextBoxToNumberOfServiceSimulation.Text) - int.Parse(TextBoxFromNumberOfServiceSimulation.Text);

            int countreplication = int.Parse(TextBoxNumberOfReplicationsSimulation.Text);
            OneStep = 100.0 / (count * countreplication + countreplication);
            count++;

            SeriesCollectionForExperiments.Add(new ColumnSeries
            {
                Values = new ChartValues<ObservablePoint>(),
            });
            SeriesCollectionForExperiments.Add(new ColumnSeries
            {
                Values = new ChartValues<ObservablePoint>(),
            });

            Task[] tasks = new Task[count];

            int bus = int.Parse(TextBoxFromNumberOfBusSimulation.Text);
            int service = int.Parse(TextBoxFromNumberOfServiceSimulation.Text);

            CreateStatisticsPanel(count, bus, service);

            for (int i = 0; i < count; i++)
            {
                MySimulation simulation = new MySimulation();
                simulation.NumberOfBus = bus;
                simulation.NumberOfService = service;
                ComboBoxItem item = (ComboBoxItem)ComboBoxBusTypeExperiment.SelectedItem;
                simulation.SetBusType(item.Tag.ToString());
                simulation.Id = i;
                simulation.SetMaxSimSpeed();

                RegisterEventsSimulation(simulation);

                tasks[i] = Task.Run(() => { simulation.Simulate(countreplication, SimulateTime); });

                Simulations.Add(simulation);

                SeriesCollectionForExperiments[0].Values.Add(new ObservablePoint(simulation.Id, 0));
                SeriesCollectionForExperiments[1].Values.Add(new ObservablePoint(simulation.Id, 0));

                if (RadioButtonFixedService.IsChecked.Value) { bus++; } else { service++; }
            }

            await Task.WhenAll(tasks);

            watch.Stop();
            EndExperiment(watch.ElapsedMilliseconds);
        }

        private void CreateStatisticsPanel(int count, int bus, int service)
        {
            for (int i = 0; i < count; i++)
            {
                StackPanel panel = new StackPanel() { Orientation = Orientation.Vertical };
                Label title = new Label() { Content = $"Cars: {bus} and Service: {service}", FontWeight = FontWeights.Bold };

                StackPanel panelMin = new StackPanel() { Orientation = Orientation.Horizontal };
                Label minTitle = new Label() { Content = "Min time to lend car:" };
                Label min = new Label() { Content = "0" };
                panelMin.Children.Add(minTitle);
                panelMin.Children.Add(min);

                StackPanel panelMax = new StackPanel() { Orientation = Orientation.Horizontal };
                Label maxTitle = new Label() { Content = "Max time to lend car:" };
                Label max = new Label() { Content = "0" };
                panelMax.Children.Add(maxTitle);
                panelMax.Children.Add(max);

                StackPanel panelAvg = new StackPanel() { Orientation = Orientation.Horizontal };
                Label avgTitle = new Label() { Content = "Average time to lend car:" };
                Label average = new Label() { Content = "0" };
                panelAvg.Children.Add(avgTitle);
                panelAvg.Children.Add(average);

                StackPanel panelInterval = new StackPanel() { Orientation = Orientation.Horizontal };
                Label intervalTitle = new Label() { Content = "Interval 90:" };
                Label interval = new Label() { Content = "< , >" };
                panelInterval.Children.Add(intervalTitle);
                panelInterval.Children.Add(interval);

                //#######################

                StackPanel panelMinReturn = new StackPanel() { Orientation = Orientation.Horizontal };
                Label minTitleReturn = new Label() { Content = "Min time to return car:" };
                Label minReturn = new Label() { Content = "0" };
                panelMinReturn.Children.Add(minTitleReturn);
                panelMinReturn.Children.Add(minReturn);

                StackPanel panelMaxReturn = new StackPanel() { Orientation = Orientation.Horizontal };
                Label maxTitleReturn = new Label() { Content = "Max time to return car:" };
                Label maxReturn = new Label() { Content = "0" };
                panelMaxReturn.Children.Add(maxTitleReturn);
                panelMaxReturn.Children.Add(maxReturn);

                StackPanel panelAvgReturn = new StackPanel() { Orientation = Orientation.Horizontal };
                Label avgTitleReturn = new Label() { Content = "Average time to return car:" };
                Label averageReturn = new Label() { Content = "0" };
                panelAvgReturn.Children.Add(avgTitleReturn);
                panelAvgReturn.Children.Add(averageReturn);

                StackPanel panelIntervalReturn = new StackPanel() { Orientation = Orientation.Horizontal };
                Label intervalTitleReturn = new Label() { Content = "Interval 90:" };
                Label intervalReturn = new Label() { Content = "< , >" };
                panelIntervalReturn.Children.Add(intervalTitleReturn);
                panelIntervalReturn.Children.Add(intervalReturn);

                //#######################

                StackPanel panelMoneyMonth = new StackPanel() { Orientation = Orientation.Horizontal };
                Label moneyMonthTitle = new Label() { Content = "Money for month:" };
                Label moneyMonth = new Label() { Content = "0 €" };
                panelMoneyMonth.Children.Add(moneyMonthTitle);
                panelMoneyMonth.Children.Add(moneyMonth);

                StackPanel panelMoneySim = new StackPanel() { Orientation = Orientation.Horizontal };
                Label moneySimTitle = new Label() { Content = "Money for day:" };
                Label moneySim = new Label() { Content = "0 €" };
                panelMoneySim.Children.Add(moneySimTitle);
                panelMoneySim.Children.Add(moneySim);

                //#######################

                ProgressBar progressBar = new ProgressBar() { Width = 200, Height = 15, Minimum = 0, Maximum = 100, Value = 0 };
                panel.Children.Add(title);
                panel.Children.Add(panelMin);
                panel.Children.Add(panelMax);
                panel.Children.Add(panelAvg);
                panel.Children.Add(panelInterval);

                panel.Children.Add(panelMinReturn);
                panel.Children.Add(panelMaxReturn);
                panel.Children.Add(panelAvgReturn);
                panel.Children.Add(panelIntervalReturn);

                panel.Children.Add(panelMoneyMonth);
                panel.Children.Add(panelMoneySim);

                panel.Children.Add(progressBar);
                Statistics.Children.Add(panel);

                if (RadioButtonFixedService.IsChecked.Value)
                {
                    bus++;
                }
                else
                {
                    service++;
                }
            }
        }

        private void EndExperiment(long time)
        {
            Window.Cursor = Cursors.Arrow;
            ButtonPauseSimulation.IsEnabled = false;
            TimeOfExperiment.Content = "Time: " + GetNiceTime((time / 1000));
            ButtonStopSimulation.IsEnabled = true;
        }

        private void RegisterEventsSimulation(MySimulation simulationObj)
        {
            // TODO
            simulationObj.UpdateResults += (simulation) =>
            {
                Dispatcher.Invoke(() =>
                {
                    double progress = ((double)simulation.CurrentReplication + 1) / simulation.ReplicationCount * 100;
                    int x = RadioButtonFixedService.IsChecked.Value ? simulation.NumberOfBus : simulation.NumberOfService;
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[1]).Children[1]).Content = GetNiceTime(simulation.TimeInSystemLend.MinValue);
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[2]).Children[1]).Content = GetNiceTime(simulation.TimeInSystemLend.MaxValue);
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[3]).Children[1]).Content = GetNiceTime(simulation.TimeInSystemLend.GetAverage());
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[4]).Children[1]).Content
                        = "<" + GetNiceTime(simulation.TimeInSystemLend.GetLeftInterval90()) + " , " + GetNiceTime(simulation.TimeInSystemLend.GetRightInterval90()) + ">";

                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[5]).Children[1]).Content = GetNiceTime(simulation.TimeInSystemReturn.MinValue);
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[6]).Children[1]).Content = GetNiceTime(simulation.TimeInSystemReturn.MaxValue);
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[7]).Children[1]).Content = GetNiceTime(simulation.TimeInSystemReturn.GetAverage());
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[8]).Children[1]).Content
                        = "<" + GetNiceTime(simulation.TimeInSystemReturn.GetLeftInterval90()) + " , " + GetNiceTime(simulation.TimeInSystemReturn.GetRightInterval90()) + ">";

                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[9]).Children[1]).Content = simulation.MonthMoneyStat.GetAverage().ToString("F") + " €";
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[10]).Children[1]).Content = simulation.SimulationMoneyStat.GetAverage().ToString("F") + " €";

                    ((ProgressBar)((StackPanel)Statistics.Children[simulation.Id]).Children[11]).Value = progress;

                    SeriesCollectionForExperiments[0].Values[simulation.Id] = new ObservablePoint(x, simulation.TimeInSystemLend.GetAverage() / 60);
                    SeriesCollectionForExperiments[1].Values[simulation.Id] = new ObservablePoint(x, simulation.TimeInSystemReturn.GetAverage() / 60);

                    ProgressBarSimulation.Value += OneStep;
                });
            };
        }

        private void ResetSimulation()
        {
            ProgressBarSimulation.Value = 0;
            Statistics.Children.Clear();
        }

        private void ButtonPauseSimulation_OnClick(object sender, RoutedEventArgs e)
        {
            Window.Cursor = Cursors.Arrow;
            ButtonStartSimulation.IsEnabled = false;
            ButtonContinueSimulation.IsEnabled = true;
            ButtonPauseSimulation.IsEnabled = false;
            ButtonStopSimulation.IsEnabled = true;

            foreach (var simulation in Simulations)
            {
                simulation?.PauseSimulation();
            }
        }

        private void ButtonContinueSimulation_OnClick(object sender, RoutedEventArgs e)
        {
            Window.Cursor = Cursors.AppStarting;
            ButtonStartSimulation.IsEnabled = false;
            ButtonContinueSimulation.IsEnabled = false;
            ButtonPauseSimulation.IsEnabled = true;
            ButtonStopSimulation.IsEnabled = false;

            foreach (var simulation in Simulations)
            {
                simulation?.ResumeSimulation();
            }
        }

        private void ButtonStopSimulation_OnClick(object sender, RoutedEventArgs e)
        {
            Window.Cursor = Cursors.Arrow;
            TimeOfExperiment.Content = "Time: 00:00:00";
            SeriesCollectionForExperiments.Clear();
            InitSimulation();

            foreach (var simulation in Simulations)
            {
                simulation?.StopSimulation();
            }

            ResetSimulation();
        }

        private void RadioButtonFixedBus_Checked(object sender, RoutedEventArgs e)
        {
            TextBoxFromNumberOfServiceSimulation.IsEnabled = true;
            TextBoxToNumberOfServiceSimulation.IsEnabled = true;
            TextBoxFromNumberOfBusSimulation.IsEnabled = true;
            TextBoxToNumberOfBusSimulation.IsEnabled = false;
        }

        private void RadioButtonFixedService_Checked(object sender, RoutedEventArgs e)
        {
            TextBoxFromNumberOfServiceSimulation.IsEnabled = true;
            TextBoxToNumberOfServiceSimulation.IsEnabled = false;
            TextBoxFromNumberOfBusSimulation.IsEnabled = true;
            TextBoxToNumberOfBusSimulation.IsEnabled = true;
        }
    }
}
