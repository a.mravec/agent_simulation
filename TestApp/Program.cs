﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using simulation;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");
            MySimulation sim = new MySimulation() { NumberOfService = 3, NumberOfBus = 2};
            sim.SetBusType("A");
            sim.TestEvent += i => { Console.WriteLine(i); };
            sim.Simulate(1, 4.5*60*60);
            Console.WriteLine("Current Time: " + sim.CurrentTime);
            Console.WriteLine("End");
            Console.ReadKey();
        }
    }
}
