using System;
using OSPABA;
using simulation;
using agents;
using OSPRNG;

namespace continualAssistants
{
	//meta! id="56"
	public class SchedulerService : Scheduler
	{
        public EmpiricRNG<double> GeneratorIn { get; set; }
        public EmpiricRNG<double> GeneratorOut { get; set; }

		public SchedulerService(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
            //TODO maybe error with random numbers / random generator
		    EmpiricPair<double> firstIn = new EmpiricPair<double>(new TriangularRNG(1.6 * 60, 2.02 * 60, 3 * 60), 0.765);
		    EmpiricPair<double> secondIn = new EmpiricPair<double>(new TriangularRNG(3 * 60, 4.62 * 60, 5.1 * 60), 0.235);
            GeneratorIn = new EmpiricRNG<double>(firstIn, secondIn);

		    EmpiricPair<double> firstOut = new EmpiricPair<double>(new TriangularRNG(1 * 60, 1.25 * 60, 2.1 * 60), 0.866);
		    EmpiricPair<double> secondOut = new EmpiricPair<double>(new TriangularRNG(2.9 * 60, 4.3 * 60, 4.8 * 60), 0.134);
		    GeneratorOut = new EmpiricRNG<double>(firstOut, secondOut);
        }

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication
		}

		//meta! sender="AgentRental", id="57", type="Start"
		public void ProcessStart(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage) message;
		    myMessage.Code = Mc.Finish;

		    if (myMessage.Group == null)
		    {
                throw new Exception("Bad");
		    }

		    double time = 0;
		    if (myMessage.Group.LendCar)
		    {
		        time = GeneratorIn.Sample();
		    }
		    else
		    {
		        time = GeneratorOut.Sample();
            }

		    Hold(time, myMessage);
        }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			    case Mc.Finish:
                    Notice(message);
			    break;
            }
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentRental MyAgent
		{
			get
			{
				return (AgentRental)base.MyAgent;
			}
		}
	}
}