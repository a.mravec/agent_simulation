using System;
using System.Runtime.InteropServices;
using OSPABA;
using simulation;
using agents;
using EventSimulation.Generators;

namespace continualAssistants
{
	//meta! id="7"
	public class SchedulerArrivalTerminalFirst : Scheduler
	{
        public GExponential Generator { get; set; }

        public SchedulerArrivalTerminalFirst(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
		    Generator = new GExponential(new[] { 4, 8, 12, 15, 18, 14, 13, 10, 4, 6, 10, 14, 16, 15, 7, 3, 4, 2 }, (MySimulation)MySim);
        }

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication
		}

		//meta! sender="AgentOkolie", id="8", type="Start"
		public void ProcessStart(MessageForm message)
		{
		    message.Code = Mc.Finish;
		    MySimulation mySimulation = (MySimulation)MySim;
		    if (mySimulation.CurrentTime < mySimulation.EndSimulation)
		    {
		        Hold(Generator.GetNumber(), message); //TODO �as pr�chodu
		    }
		}

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
                case Mc.Finish:
                    MessageForm mess = message.CreateCopy();
                    Notice(mess);
                    MySimulation mySimulation = (MySimulation)MySim;
                    if (mySimulation.CurrentTime < mySimulation.EndSimulation)
                    {
                        Hold(Generator.GetNumber(), message); //TODO �as pr�chodu
                    }
                    break;
			}
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentOkolie MyAgent
		{
			get
			{
				return (AgentOkolie)base.MyAgent;
			}
		}
	}
}