using System;
using OSPABA;
using simulation;
using agents;
using Core.entities;

namespace continualAssistants
{
	//meta! id="67"
	public class SchedulerMoveBus : Scheduler
	{
		public SchedulerMoveBus(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication
		}

		//meta! sender="AgentBus", id="68", type="Start"
		public void ProcessStart(MessageForm message)
		{
		    double time = 0;
		    MyMessage myMessage = (MyMessage)message;
		    myMessage.Code = Mc.Finish;

		    MySimulation mySimulation = (MySimulation) MySim;
		    if (mySimulation.CurrentTime > mySimulation.EndSimulation &&
		        mySimulation.AgentOkolie.Customers <= mySimulation.AgentOkolie.EndCustomer && 
		        myMessage.Bus.Route == Routes.RentalToFirst)
		    {
		        MyAgent.BusInStation++;
		        mySimulation.Basic();
		        return;
		    }

		    switch (myMessage.Bus.Route)
		    {
                case Routes.RentalToFirst:
                    time = (2.5 / 35) * 3600;
                    myMessage.Bus.Distance += 2.5;
                    break;

		        case Routes.FirstToSecond:
		            time = (0.5 / 35) * 3600;
		            myMessage.Bus.Distance += 0.5;
                    break;

		        case Routes.SecondToRental:
		            time = (3.4 / 35) * 3600;
		            myMessage.Bus.Distance += 3.4;
                    break;

		        case Routes.RentalToThird:
		            time = (2.9 / 35) * 3600;
		            myMessage.Bus.Distance += 2.9;
                    break;

		        case Routes.ThirdToFirst:
		            time = (0.9 / 35) * 3600;
		            myMessage.Bus.Distance += 0.9;
                    break;
                default:
                    throw new Exception("Bad value of enum.");
                    break;
            }

		    Hold(time, myMessage);
        }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			    case Mc.Finish:
			        Notice(message);
			    break;
            }
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentBus MyAgent
		{
			get
			{
				return (AgentBus)base.MyAgent;
			}
		}
	}
}