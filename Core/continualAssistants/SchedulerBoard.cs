using System;
using OSPABA;
using simulation;
using agents;
using Core.entities;
using OSPDataStruct;
using OSPRNG;

namespace continualAssistants
{
	//meta! id="84"
	public class SchedulerBoard : Scheduler
	{
	    public UniformContinuousRNG Generator { get; set; }

        public SchedulerBoard(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
            // Setup component for the next replication
		    Generator = new UniformContinuousRNG(10, 14);
        }

		//meta! sender="AgentQueue", id="85", type="Start"
		public void ProcessStart(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    myMessage.Code = Mc.Finish;
		    double time = 0;
		    myMessage.Group = GetGroup(myMessage);
		    if (myMessage.Group != null)
		    {
		        time = GetTime(myMessage.Group.NumberCustomer);
		    }

            Hold(time, myMessage);
        }

	    public Group GetGroup(MessageForm message)
	    {
	        MyMessage myMessage = (MyMessage)message;
	        Group group = null;
	        //int emptyPLaces = myMessage.Bus.Type.Capacity - myMessage.Bus.OccupiedPlaces;
	        //if (emptyPLaces < 0)
	        //{
         //       throw new Exception("Bad");
	        //}
            switch (myMessage.Bus.Route)
	        {
	            case Routes.RentalToFirst:
	                group = MyAgent.TerminalFirstDequeue(myMessage.Bus);
                    break;

	            //case Routes.ThirdToFirst:
	            //    group = GetGroupByLenght(MyAgent.TerminalFirst, emptyPLaces);
	            //    break;

                case Routes.FirstToSecond:
	                group = MyAgent.TerminalSecondDequeue(myMessage.Bus);
                    break;

	            case Routes.SecondToRental:
	                group = MyAgent.RentalOutDequeue(myMessage.Bus);
                    break;

	            default:
	                throw new Exception("Bad");
	                break;
	        }

	        return group;
	    }
        
	    //public Group GetGroupByLenght(SimQueue<Group> queue, int length)
	    //{
	    //    Group group = null;

     //       foreach (Group g in queue)
	    //    {
	    //        if (g.NumberCustomer <= length)
	    //        {
	    //            group = g;
	    //            break;
	    //        }
	    //    }

	    //    if (group != null && !queue.Remove(group))
	    //    {
     //           throw new Exception("Bad");
	    //    } // TODO presunut inde

	    //    return group;
	    //}

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			    case Mc.Finish:
			        MyMessage myMessage = (MyMessage)message;
			        MyMessage mess = (MyMessage)message.CreateCopy();
			        Notice(mess);

			        if (myMessage.Group != null)
			        {
			            double time = 0;
			            myMessage.Group = GetGroup(myMessage);
			            if (myMessage.Group != null)
			            {
			                time = GetTime(myMessage.Group.NumberCustomer);
			            }
                        Hold(time, myMessage);
			        }
			        break;
            }
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentQueue MyAgent
		{
			get
			{
				return (AgentQueue)base.MyAgent;
			}
		}

	    public double GetTime(int count)
	    {
	        double time = 0;
	        for (int i = 0; i < count; i++)
	        {
	            time += Generator.Sample();
	        }
	        return time;
	    }
    }
}