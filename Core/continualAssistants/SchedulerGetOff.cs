using OSPABA;
using simulation;
using agents;
using OSPRNG;

namespace continualAssistants
{
	//meta! id="86"
	public class SchedulerGetOff : Scheduler
	{
        public UniformContinuousRNG Generator { get; set; }

        public SchedulerGetOff(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
            // Setup component for the next replication
		    Generator = new UniformContinuousRNG(2,10);
        }

		//meta! sender="AgentQueue", id="87", type="Start"
		public void ProcessStart(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage) message; // TODO skontrolovať vystupovanie
		    myMessage.Code = Mc.Finish;

		    if (myMessage.Bus.BusQueue.IsEmpty())
		    {
		        myMessage.Group = null;
                Notice(myMessage);
            }
		    else
		    {
		        Hold(GetTime(myMessage.Bus.BusQueue.Peek().NumberCustomer), myMessage);
            }
        }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			    case Mc.Finish:
			        MyMessage myMessage = (MyMessage) message;
                    
			        if (!myMessage.Bus.BusQueue.IsEmpty())
			        {
			            myMessage.Group = myMessage.Bus.Dequeue();
                    }
			        MyMessage mess = (MyMessage)message.CreateCopy();
                    Notice(mess);



			        if (myMessage.Group != null)
			        {
			            double time = 0;

			            if (!myMessage.Bus.BusQueue.IsEmpty())
			            {
			                time = GetTime(myMessage.Group.NumberCustomer);
			            }
			            else
			            {
			                myMessage.Group = null;
			            }

			            Hold(time, myMessage);
			        }

                    break;
            }
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentQueue MyAgent
		{
			get
			{
				return (AgentQueue)base.MyAgent;
			}
		}

	    public double GetTime(int count)
	    {
	        double time = 0;
	        for (int i = 0; i < count; i++)
	        {
	            time += Generator.Sample();
	        }
	        return time;
	    }
	}
}