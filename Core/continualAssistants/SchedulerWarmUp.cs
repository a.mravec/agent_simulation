using OSPABA;
using simulation;
using agents;
namespace continualAssistants
{
	//meta! id="90"
	public class SchedulerWarmUp : Scheduler
	{
		public SchedulerWarmUp(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication
		}

		//meta! sender="AgentModel", id="91", type="Start"
		public void ProcessStart(MessageForm message)
		{
		    message.Code = Mc.Finish;
		    MySimulation mySimulation = (MySimulation) MySim;
            Hold(mySimulation.WarmUpTime, message);
        }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			    case Mc.Finish:
			        Notice(message);
			        break;
            }
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentModel MyAgent
		{
			get
			{
				return (AgentModel)base.MyAgent;
			}
		}
	}
}
