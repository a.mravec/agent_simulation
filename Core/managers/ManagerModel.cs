using OSPABA;
using simulation;
using agents;
using continualAssistants;
using instantAssistants;
namespace managers
{
	//meta! id="1"
	public class ManagerModel : Manager
	{
		public ManagerModel(int id, Simulation mySim, Agent myAgent) :
			base(id, mySim, myAgent)
		{
			Init();
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication

			if (PetriNet != null)
			{
				PetriNet.Clear();
			}
		}

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			}
		}

		//meta! sender="SchedulerWarmUp", id="91", type="Finish"
		public void ProcessFinish(MessageForm message)
		{
		    message.Code = Mc.ResetStats;

		    MessageForm messageForm = message.CreateCopy();
		    messageForm.AddresseeId = SimId.AgentQueue;
            Notice(messageForm);

		    messageForm = message.CreateCopy();
		    messageForm.AddresseeId = SimId.AgentBus;
		    Notice(messageForm);

		    messageForm = message.CreateCopy();
		    messageForm.AddresseeId = SimId.AgentRental;
		    Notice(messageForm);

		    messageForm = message.CreateCopy();
		    messageForm.AddresseeId = SimId.AgentOkolie;
		    Notice(messageForm);

		    ((MySimulation) MySim).SimulationMoney = 0;
		    ((MySimulation) MySim).MonthMoney = 0;
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		public void Init()
		{
		}

		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Finish:
				ProcessFinish(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentModel MyAgent
		{
			get
			{
				return (AgentModel)base.MyAgent;
			}
		}
	}
}