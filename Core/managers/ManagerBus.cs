using System;
using OSPABA;
using simulation;
using agents;
using continualAssistants;
using Core.entities;
using instantAssistants;
namespace managers
{
	//meta! id="63"
	public class ManagerBus : Manager
	{
		public ManagerBus(int id, Simulation mySim, Agent myAgent) :
			base(id, mySim, myAgent)
		{
			Init();
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication

			if (PetriNet != null)
			{
				PetriNet.Clear();
			}
		}

		//meta! sender="AgentModel", id="74", type="Notice"
		public void ProcessInit(MessageForm message)
		{
		    for (int i = 0; i < MyAgent.NumberOfBus; i++)
		    {
		        Bus bus = new Bus(MySim);
		        bus.Type = ((MySimulation) MySim).BusType;
                MyAgent.Busses.Add(bus);

		        MyMessage myMessage = (MyMessage) message.CreateCopy();
		        myMessage.Bus = bus;
		        myMessage.AddresseeId = SimId.SchedulerMoveBus;
		        StartContinualAssistant(myMessage);
            }
		}

		//meta! sender="AgentQueue", id="71", type="Response"
		public void ProcessBoardTerminalSecond(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    myMessage.Bus.Route = Routes.SecondToRental;
		    myMessage.AddresseeId = SimId.SchedulerMoveBus;
		    StartContinualAssistant(myMessage);
        }

		//meta! sender="AgentQueue", id="73", type="Response"
		public void ProcessBoardTerminalFirst(MessageForm message)
		{
            MyMessage myMessage = (MyMessage)message;
		    myMessage.Bus.Route = Routes.FirstToSecond;
		    myMessage.AddresseeId = SimId.SchedulerMoveBus;
		    StartContinualAssistant(myMessage);
        }

		//meta! sender="SchedulerMoveBus", id="68", type="Finish"
		public void ProcessFinish(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | BUS (" + myMessage.Bus.Id + ") | Arrival: " + myMessage.Bus.Route);
            switch (myMessage.Bus.Route)
		    {
		        case Routes.RentalToFirst:
                    myMessage.AddresseeId = SimId.AgentQueue;
		            myMessage.Code = Mc.BoardTerminalFirst;
		            Request(myMessage);
                    break;

		        case Routes.FirstToSecond:
		            myMessage.AddresseeId = SimId.AgentQueue;
		            myMessage.Code = Mc.BoardTerminalSecond;
		            Request(myMessage);
                    break;

		        case Routes.SecondToRental:
		            myMessage.AddresseeId = SimId.AgentQueue;
		            myMessage.Code = Mc.BoardRental;
		            Request(myMessage);
                    break;

		        case Routes.RentalToThird:
		            myMessage.AddresseeId = SimId.AgentQueue;
		            myMessage.Code = Mc.GetOffTerminalThird;
		            Request(myMessage);
                    break;

		        case Routes.ThirdToFirst:
		            myMessage.AddresseeId = SimId.AgentQueue;
		            myMessage.Code = Mc.BoardTerminalFirst;
		            myMessage.Bus.Route = Routes.RentalToFirst; // arrival to terminal first
                    Request(myMessage);
                    break;
		        default:
		            throw new NotImplementedException("Bad value of enum.");
		            break;
		    }
        }

		//meta! sender="AgentQueue", id="72", type="Response"
		public void ProcessBoardRental(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    if (myMessage.Bus.BusQueue.IsEmpty())
		    {
		        myMessage.Bus.Route = Routes.RentalToFirst;
		        myMessage.AddresseeId = SimId.SchedulerMoveBus;
		        StartContinualAssistant(myMessage);
            }
		    else
		    {
		        myMessage.Bus.Route = Routes.RentalToThird;
		        myMessage.AddresseeId = SimId.SchedulerMoveBus;
		        StartContinualAssistant(myMessage);
            }
        }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			}
		}

		//meta! sender="AgentQueue", id="77", type="Response"
		public void ProcessGetOffTerminalThird(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    myMessage.Bus.Route = Routes.ThirdToFirst;
		    myMessage.AddresseeId = SimId.SchedulerMoveBus;
		    StartContinualAssistant(myMessage);
        }

		//meta! sender="AgentModel", id="92", type="Notice"
		public void ProcessResetStats(MessageForm message)
		{
		    foreach (Bus bus in MyAgent.Busses)
		    {
		        bus.BusStat.Reset();
		        bus.Distance = 0;
		    }
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		public void Init()
		{
		}

		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Finish:
				ProcessFinish(message);
			break;

			case Mc.GetOffTerminalThird:
				ProcessGetOffTerminalThird(message);
			break;

			case Mc.BoardTerminalFirst:
				ProcessBoardTerminalFirst(message);
			break;

			case Mc.BoardRental:
				ProcessBoardRental(message);
			break;

			case Mc.ResetStats:
				ProcessResetStats(message);
			break;

			case Mc.BoardTerminalSecond:
				ProcessBoardTerminalSecond(message);
			break;

			case Mc.Init:
				ProcessInit(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentBus MyAgent
		{
			get
			{
				return (AgentBus)base.MyAgent;
			}
		}
	}
}