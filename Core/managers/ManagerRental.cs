using OSPABA;
using simulation;
using agents;
using continualAssistants;
using instantAssistants;
namespace managers
{
	//meta! id="47"
	public class ManagerRental : Manager
	{
		public ManagerRental(int id, Simulation mySim, Agent myAgent) :
			base(id, mySim, myAgent)
		{
			Init();
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication

			if (PetriNet != null)
			{
				PetriNet.Clear();
			}
		}

		//meta! sender="AgentQueue", id="53", type="Response"
		public void ProcessGetNextCustomer(MessageForm message)
		{
		    MyAgent.UpdateStat();
            MyMessage myMessage = (MyMessage)message;
		    if (myMessage.Group != null)
		    {
                // scheduler 
		        ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | RENTAL (" + MyAgent.FreeNumberOfService + ") | Work");
                message.Addressee = MyAgent.FindAssistant(SimId.SchedulerService);
		        StartContinualAssistant(message);
            }
		    else
		    {
		        MyAgent.FreeNumberOfService++;
		    }
        }

		//meta! sender="AgentQueue", id="49", type="Request"
		public void ProcessHaveFreeService(MessageForm message)
		{
		    MyAgent.UpdateStat();
            MyMessage myMessage = (MyMessage)message;
            if (MyAgent.FreeNumberOfService > 0)
            {
                myMessage.FreeService = true;
                MyAgent.FreeNumberOfService--;
            }
		    else
		    {
		        myMessage.FreeService = false;
            }
            Response(myMessage);
		}

		//meta! sender="AgentQueue", id="51", type="Notice"
		public void ProcessUseService(MessageForm message)
		{
            // scheduler
		    ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | RENTAL (" + MyAgent.FreeNumberOfService + ") | Work");
            message.Addressee = MyAgent.FindAssistant(SimId.SchedulerService);
		    StartContinualAssistant(message);
        }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			}
		}

		//meta! sender="SchedulerService", id="57", type="Finish"
		public void ProcessFinish(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    if (!myMessage.Group.LendCar)
		    {
		        myMessage.Code = Mc.AddToRentalOut;
		        myMessage.AddresseeId = SimId.AgentQueue;
                Notice(myMessage);
            }
		    else
		    {
                // send agent okolie customer end in system
		        myMessage.Code = Mc.EndCustomer;
		        myMessage.AddresseeId = SimId.AgentOkolie;
		        Notice(myMessage);
            }

		    ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | RENTAL (" + (MyAgent.FreeNumberOfService + 1) + ") | Finish");

            MyMessage newMyMessage = (MyMessage) myMessage.CreateCopy();
            newMyMessage.Code = Mc.GetNextCustomer;
		    newMyMessage.AddresseeId = SimId.AgentQueue;
            Request(newMyMessage);
		}

		//meta! sender="AgentModel", id="93", type="Notice"
		public void ProcessResetStats(MessageForm message)
		{
            MyAgent.ServiceStat.Reset();
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		public void Init()
		{
		}

		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.GetNextCustomer:
				ProcessGetNextCustomer(message);
			break;

			case Mc.UseService:
				ProcessUseService(message);
			break;

			case Mc.Finish:
				ProcessFinish(message);
			break;

			case Mc.ResetStats:
				ProcessResetStats(message);
			break;

			case Mc.HaveFreeService:
				ProcessHaveFreeService(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentRental MyAgent
		{
			get
			{
				return (AgentRental)base.MyAgent;
			}
		}
	}
}