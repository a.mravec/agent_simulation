using System;
using OSPABA;
using simulation;
using agents;
using continualAssistants;
using Core.entities;
using instantAssistants;
using Action = OSPABA.Action;

namespace managers
{
	//meta! id="29"
	public class ManagerQueue : Manager
	{
		public ManagerQueue(int id, Simulation mySim, Agent myAgent) :
			base(id, mySim, myAgent)
		{
			Init();
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication

			if (PetriNet != null)
			{
				PetriNet.Clear();
			}
		}

		//meta! sender="AgentOkolie", id="31", type="Notice"
		public void ProcessAddToTerminalFirst(MessageForm message)
		{
            ((Action)MyAgent.FindAssistant(SimId.ActionAddTerminalFirst)).Execute(message);
        }

		//meta! sender="AgentOkolie", id="32", type="Notice"
		public void ProcessAddToTerminalSecond(MessageForm message)
		{
		    ((Action)MyAgent.FindAssistant(SimId.ActionAddTerminalSecond)).Execute(message);
        }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			}
		}

		//meta! sender="AgentOkolie", id="42", type="Notice"
		public void ProcessAddToRental(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    myMessage.Code = Mc.HaveFreeService;
		    myMessage.AddresseeId = SimId.AgentRental;
            Request(myMessage);
		}

		//meta! sender="AgentRental", id="53", type="Request"
		public void ProcessGetNextCustomer(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    if (MyAgent.RentalIn.Count > 0)
		    {
		        myMessage.Group = MyAgent.RentalInDequeue();
		    }
		    else
		    {
		        myMessage.Group = null;
            }
		    Response(myMessage);
        }

		//meta! sender="AgentRental", id="49", type="Response"
		public void ProcessHaveFreeService(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    if (myMessage.FreeService)
		    {
		        myMessage.Code = Mc.UseService;
		        myMessage.AddresseeId = SimId.AgentRental;
                Notice(myMessage);
		    }
		    else
		    {
		        ((Action)MyAgent.FindAssistant(SimId.ActionAddRental)).Execute(message);
            }
        }

		//meta! sender="AgentRental", id="52", type="Notice"
		public void ProcessAddToRentalOut(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    MyAgent.RentalOutEnqueue(myMessage.Group);
		    ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | TERMINAL | Terminal OUT: " + MyAgent.RentalOut.Count);
        }

		//meta! sender="AgentBus", id="71", type="Request"
		public void ProcessBoardTerminalSecond(MessageForm message)
		{
            // boarding in second terminal
		    ProcessBoarding(message);
        }

		//meta! sender="AgentBus", id="73", type="Request"
		public void ProcessBoardTerminalFirst(MessageForm message)
		{
            // boarding in first terminal
		    ProcessBoarding(message);
        }

		//meta! sender="AgentBus", id="72", type="Request"
		public void ProcessBoardRental(MessageForm message)
		{
            // get off
		    ProcessGetOff(message);
        }

		//meta! sender="AgentBus", id="77", type="Request"
		public void ProcessGetOffTerminalThird(MessageForm message)
		{
            // get off in third terminal
		    ProcessGetOff(message);
		}

		//meta! sender="SchedulerBoard", id="85", type="Finish"
		public void ProcessFinishSchedulerBoard(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    if (myMessage.Group != null)
		    {
		        myMessage.Bus.Enqueue(myMessage.Group);
            }
		    else
		    {
		        if (myMessage.Bus.Route == Routes.RentalToFirst)
		        {
                    //myMessage.Bus.Route = Routes.FirstToSecond;
		            myMessage.Code = Mc.BoardTerminalFirst;
		        }
		        else if (myMessage.Bus.Route == Routes.FirstToSecond)
		        {
                    //myMessage.Bus.Route = Routes.SecondToRental;
		            myMessage.Code = Mc.BoardTerminalSecond;
                }
		        else if (myMessage.Bus.Route == Routes.SecondToRental)
		        {
		            myMessage.Code = Mc.BoardRental;
                    if (myMessage.Bus.BusQueue.IsEmpty())
		            {
                        //myMessage.Bus.Route = Routes.RentalToFirst;
                    }
		            else
		            {
		                //myMessage.Bus.Route = Routes.RentalToThird;
                    }
		        }
		        else if (myMessage.Bus.Route == Routes.RentalToThird)
		        {
                    //myMessage.Bus.Route = Routes.RentalToFirst;
		            myMessage.Code = Mc.GetOffTerminalThird;
                }

                Response(myMessage);
		    }
        }

		//meta! sender="SchedulerGetOff", id="87", type="Finish"
		public void ProcessFinishSchedulerGetOff(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    if (myMessage.Group != null)
		    {
		        if (myMessage.Bus.Route == Routes.RentalToThird)
		        {
		            MyMessage endMessage = (MyMessage) myMessage.CreateCopy();
		            endMessage.Code = Mc.EndCustomer;
		            endMessage.AddresseeId = SimId.AgentOkolie;
		            Notice(endMessage);
		            //if (myMessage.Bus.BusQueue.IsEmpty())
		            //{
		            //    myMessage.Code = Mc.GetOffTerminalThird;
              //          Response(myMessage);
		            //}
		        }
		        else if (myMessage.Bus.Route == Routes.SecondToRental)
		        {
		            MyMessage rentalMessage = (MyMessage)message;
		            rentalMessage.Code = Mc.HaveFreeService;
		            rentalMessage.AddresseeId = SimId.AgentRental;
		            Request(rentalMessage);
                }
		        else
		        {
		            throw new Exception("Bad");
		        }
		    }
		    else
		    {
		        if (myMessage.Bus.Route == Routes.RentalToThird)
		        {
		            myMessage.Code = Mc.GetOffTerminalThird;
                    Response(myMessage);
                }
		        else if (myMessage.Bus.Route == Routes.SecondToRental)
                {
                    ProcessBoarding(myMessage);
		        }
		        else
		        {
		            throw new Exception("Bad, Time: " + MySim.CurrentTime);
		        }
            }
		}

		//meta! sender="AgentModel", id="95", type="Notice"
		public void ProcessResetStats(MessageForm message)
		{
            MyAgent.RentalInLengthStat.Reset();
            MyAgent.RentalInTimeStat.Reset();

            MyAgent.RentalOutLengthStat.Reset();
            MyAgent.RentalOutTimeStat.Reset();

            MyAgent.TerminalFirstLengthStat.Reset();
            MyAgent.TerminalFirstTimeStat.Reset();

            MyAgent.TerminalSecondLengthStat.Reset();
            MyAgent.TerminalSecondTimeStat.Reset();
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		public void Init()
		{
		}

		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Finish:
				switch (message.Sender.Id)
				{
				case SimId.SchedulerBoard:
					ProcessFinishSchedulerBoard(message);
				break;

				case SimId.SchedulerGetOff:
					ProcessFinishSchedulerGetOff(message);
				break;
				}
			break;

			case Mc.GetOffTerminalThird:
				ProcessGetOffTerminalThird(message);
			break;

			case Mc.GetNextCustomer:
				ProcessGetNextCustomer(message);
			break;

			case Mc.AddToTerminalSecond:
				ProcessAddToTerminalSecond(message);
			break;

			case Mc.HaveFreeService:
				ProcessHaveFreeService(message);
			break;

			case Mc.BoardTerminalSecond:
				ProcessBoardTerminalSecond(message);
			break;

			case Mc.AddToTerminalFirst:
				ProcessAddToTerminalFirst(message);
			break;

			case Mc.BoardTerminalFirst:
				ProcessBoardTerminalFirst(message);
			break;

			case Mc.ResetStats:
				ProcessResetStats(message);
			break;

			case Mc.BoardRental:
				ProcessBoardRental(message);
			break;

			case Mc.AddToRentalOut:
				ProcessAddToRentalOut(message);
			break;

			case Mc.AddToRental:
				ProcessAddToRental(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentQueue MyAgent
		{
			get
			{
				return (AgentQueue)base.MyAgent;
			}
		}

	    public void ProcessGetOff(MessageForm message)
	    {
	        message.AddresseeId = SimId.SchedulerGetOff;
	        StartContinualAssistant(message);
        }

	    public void ProcessBoarding(MessageForm message)
	    {
	        message.AddresseeId = SimId.SchedulerBoard;
	        StartContinualAssistant(message);
        }
	}
}