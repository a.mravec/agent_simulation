using System.Linq;
using OSPABA;
using simulation;
using agents;
using continualAssistants;
using Core.entities;
using instantAssistants;
namespace managers
{
	//meta! id="2"
	public class ManagerOkolie : Manager
	{
		public ManagerOkolie(int id, Simulation mySim, Agent myAgent) :
			base(id, mySim, myAgent)
		{
			Init();
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication

			if (PetriNet != null)
			{
				PetriNet.Clear();
			}
		}

		//meta! sender="AgentModel", id="6", type="Notice"
		public void ProcessInit(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message.CreateCopy();
		    myMessage.Addressee = MyAgent.FindAssistant(SimId.SchedulerArrivalTerminalFirst);
		    StartContinualAssistant(myMessage);

		    myMessage = (MyMessage)message.CreateCopy();
		    myMessage.Addressee = MyAgent.FindAssistant(SimId.SchedulerArrivalTerminalSecond);
		    StartContinualAssistant(myMessage);

		    myMessage = (MyMessage)message.CreateCopy();
            myMessage.Addressee = MyAgent.FindAssistant(SimId.SchedulerArrivalRental);
		    StartContinualAssistant(myMessage);
        }

		//meta! sender="SchedulerArrivalTerminalSecond", id="10", type="Finish"
		public void ProcessFinishSchedulerArrivalTerminalSecond(MessageForm message)
		{
		    ((Action)MyAgent.FindAssistant(SimId.ActionGenerateGroup)).Execute(message);
            // send message to terminal 
            MyMessage myMessage = (MyMessage)message.CreateCopy();
		    myMessage.AddresseeId = SimId.AgentQueue;
		    myMessage.Code = Mc.AddToTerminalSecond;
		    myMessage.Group.LendCar = true;
            Notice(myMessage);
		    MyAgent.TerminalSecond++;
		}

		//meta! sender="SchedulerArrivalRental", id="12", type="Finish"
		public void ProcessFinishSchedulerArrivalRental(MessageForm message)
		{
		    ((Action)MyAgent.FindAssistant(SimId.ActionGenerateGroup)).Execute(message);
            // send message to rental 
		    MyMessage myMessage = (MyMessage)message.CreateCopy();
		    myMessage.AddresseeId = SimId.AgentQueue;
		    myMessage.Code = Mc.AddToRental;
		    myMessage.Group.LendCar = false;
            Notice(myMessage);
		    MyAgent.Rental++;
        }

		//meta! sender="SchedulerArrivalTerminalFirst", id="8", type="Finish"
		public void ProcessFinishSchedulerArrivalTerminalFirst(MessageForm message)
		{
		    ((Action)MyAgent.FindAssistant(SimId.ActionGenerateGroup)).Execute(message);
            // send message to terminal
            MyMessage myMessage = (MyMessage)message.CreateCopy();
		    myMessage.AddresseeId = SimId.AgentQueue;
		    myMessage.Code = Mc.AddToTerminalFirst;
		    myMessage.Group.LendCar = true;
            Notice(myMessage);
		    MyAgent.TerminalFirst++;
        }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
			}
		}

		//meta! sender="AgentRental", id="59", type="Notice"
		public void ProcessEndCustomerAgentRental(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    UpdateTimeInSystemStat(myMessage.Group);
            ((MySimulation)MySim).CallUpdateTimeInSystem();
		    MyAgent.EndCustomer++;
		}

		//meta! sender="AgentQueue", id="79", type="Notice"
		public void ProcessEndCustomerAgentQueue(MessageForm message)
	    {
	        MyMessage myMessage = (MyMessage) message;
	        UpdateTimeInSystemStat(myMessage.Group);
	        ((MySimulation) MySim).CallUpdateTimeInSystem();
	        MyAgent.EndCustomer++;
	    }

	    public void UpdateTimeInSystemStat(Group group)
	    {
            for (int i = 0; i < group.NumberCustomer; i++)
            {
                if (group.LendCar)
                {
                    MyAgent.TimeInSystemLendCar.Add(MySim.CurrentTime - group.ArrivalTime);
                }
                else
                {
                    MyAgent.TimeInSystemReturnCar.Add(MySim.CurrentTime - group.ArrivalTime);
                }
            }
            //if (group.LendCar)
            //{
            //    MyAgent.TimeInSystemLendCar.Add(MySim.CurrentTime - group.ArrivalTime);
            //}
            //else
            //{
            //    MyAgent.TimeInSystemReturnCar.Add(MySim.CurrentTime - group.ArrivalTime);
            //}
        }

        //meta! sender="AgentModel", id="94", type="Notice"
        public void ProcessResetStats(MessageForm message)
		{
            MyAgent.TimeInSystemLendCar.Reset();
            MyAgent.TimeInSystemReturnCar.Reset();
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		public void Init()
		{
		}

		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Finish:
				switch (message.Sender.Id)
				{
				case SimId.SchedulerArrivalTerminalSecond:
					ProcessFinishSchedulerArrivalTerminalSecond(message);
				break;

				case SimId.SchedulerArrivalRental:
					ProcessFinishSchedulerArrivalRental(message);
				break;

				case SimId.SchedulerArrivalTerminalFirst:
					ProcessFinishSchedulerArrivalTerminalFirst(message);
				break;
				}
			break;

			case Mc.Init:
				ProcessInit(message);
			break;

			case Mc.EndCustomer:
				switch (message.Sender.Id)
				{
				case SimId.AgentQueue:
					ProcessEndCustomerAgentQueue(message);
				break;

				case SimId.AgentRental:
					ProcessEndCustomerAgentRental(message);
				break;
				}
			break;

			case Mc.ResetStats:
				ProcessResetStats(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new AgentOkolie MyAgent
		{
			get
			{
				return (AgentOkolie)base.MyAgent;
			}
		}
	}
}