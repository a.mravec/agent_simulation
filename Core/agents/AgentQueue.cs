using System;
using OSPABA;
using simulation;
using managers;
using continualAssistants;
using Core.entities;
using EventSimulation.Simulation.Statistics;
using instantAssistants;
using OSPDataStruct;
using OSPStat;

namespace agents
{
	//meta! id="29"
	public class AgentQueue : Agent
	{
        public Statistics TerminalFirstLengthStat { get; set; }
        public Statistics TerminalFirstTimeStat { get; set; }
        public SimQueue<Group> TerminalFirst { get; set; }

	    public Statistics TerminalSecondLengthStat { get; set; }
	    public Statistics TerminalSecondTimeStat { get; set; }
        public SimQueue<Group> TerminalSecond { get; set; }

	    public Statistics RentalInLengthStat { get; set; }
	    public Statistics RentalInTimeStat { get; set; }
	    public SimQueue<Group> RentalIn { get; set; }

	    public Statistics RentalOutLengthStat { get; set; }
	    public Statistics RentalOutTimeStat { get; set; }
	    public SimQueue<Group> RentalOut { get; set; }

        public AgentQueue(int id, Simulation mySim, Agent parent) :
			base(id, mySim, parent)
		{
			Init();
		    TerminalFirstLengthStat = new Statistics(MySim, false);
            TerminalFirstTimeStat = new Statistics(MySim, true);

		    TerminalSecondLengthStat = new Statistics(MySim, false);
		    TerminalSecondTimeStat = new Statistics(MySim, true);

		    RentalInLengthStat = new Statistics(MySim, false);
		    RentalInTimeStat = new Statistics(MySim, true);

		    RentalOutLengthStat = new Statistics(MySim, false);
		    RentalOutTimeStat = new Statistics(MySim, true);
        }

	    public void TerminalFirstEnqueue(Group group)
	    {
	        group.StartTimeInRentalInQueue = MySim.CurrentTime;
            TerminalFirstLengthStat.Add(TerminalFirst.Count);
            TerminalFirst.Enqueue(group);
            ((MySimulation)MySim).CallUpdateGroupToTerminalFirst(group, true);
	    }

	    public Group TerminalFirstDequeue(Bus bus)
	    {
	        Group group = null;

	        foreach (Group g in TerminalFirst)
	        {
	            if (g.NumberCustomer <= bus.Type.Capacity - bus.OccupiedPlaces)
	            {
	                group = g;
	                break;
	            }
	        }

	        if (group != null && !TerminalFirst.Remove(group))
	        {
	            throw new Exception("Bad");
	        } // TODO presunut inde

            if (group != null)
            {
                bus.OccupiedPlaces += group.NumberCustomer;
                TerminalFirstLengthStat.Add(TerminalFirst.Count + 1);
	            TerminalFirstTimeStat.Add(MySim.CurrentTime - group.StartTimeInRentalInQueue);
	            ((MySimulation) MySim).CallUpdateGroupToTerminalFirst(group, false);
	        }

	        return group;
	    }

        public void TerminalSecondEnqueue(Group group)
	    {
	        group.StartTimeInRentalInQueue = MySim.CurrentTime;
            TerminalSecondLengthStat.Add(TerminalSecond.Count);
	        TerminalSecond.Enqueue(group);
	        ((MySimulation)MySim).CallUpdateGroupToTerminalSecond(group, true);
	    }

	    public Group TerminalSecondDequeue(Bus bus)
	    {
	        Group group = null;

	        foreach (Group g in TerminalSecond)
	        {
	            if (g.NumberCustomer <= bus.Type.Capacity - bus.OccupiedPlaces)
	            {
	                group = g;
	                break;
	            }
	        }

	        if (group != null && !TerminalSecond.Remove(group))
	        {
	            throw new Exception("Bad");
	        } // TODO presunut inde

	        if (group != null)
	        {
	            bus.OccupiedPlaces += group.NumberCustomer;
                TerminalSecondLengthStat.Add(TerminalSecond.Count + 1);
	            TerminalSecondTimeStat.Add(MySim.CurrentTime - group.StartTimeInRentalInQueue);
	            ((MySimulation)MySim).CallUpdateGroupToTerminalSecond(group, false);
            }

	        return group;
        }

        public void RentalInEnqueue(Group group)
        {
            group.StartTimeInRentalInQueue = MySim.CurrentTime;
            RentalInLengthStat.Add(RentalIn.Count);
	        RentalIn.Enqueue(group);
	        ((MySimulation)MySim).CallUpdateGroupToRentalIn(group, true);
	    }

	    public Group RentalInDequeue()
	    {
	        RentalInLengthStat.Add(RentalIn.Count);
	        Group group = RentalIn.Dequeue();
            RentalInTimeStat.Add(MySim.CurrentTime - group.StartTimeInRentalInQueue);
	        ((MySimulation)MySim).CallUpdateGroupToRentalIn(group, false);

	        return group;
        }

	    public void RentalOutEnqueue(Group group)
	    {
	        group.StartTimeInRentalOutQueue = MySim.CurrentTime;
	        RentalOutLengthStat.Add(RentalOut.Count);
	        RentalOut.Enqueue(group);
	        ((MySimulation)MySim).CallUpdateGroupToRentalOut(group, true);
	    }

	    public Group RentalOutDequeue(Bus bus)
	    {
	        Group group = null;

	        foreach (Group g in RentalOut)
	        {
	            if (g.NumberCustomer <= bus.Type.Capacity - bus.OccupiedPlaces)
	            {
	                group = g;
	                break;
	            }
	        }

	        if (group != null && !RentalOut.Remove(group))
	        {
	            throw new Exception("Bad");
	        } // TODO presunut inde

	        if (group != null)
	        {
	            bus.OccupiedPlaces += group.NumberCustomer;
                RentalOutLengthStat.Add(RentalOut.Count + 1);
	            RentalOutTimeStat.Add(MySim.CurrentTime - group.StartTimeInRentalOutQueue);
	            ((MySimulation)MySim).CallUpdateGroupToRentalOut(group, false);
            }

	        return group;
	    }

        override public void PrepareReplication()
		{
			base.PrepareReplication();
            // Setup component for the next replication
		    TerminalFirstLengthStat?.Reset();
		    TerminalFirstTimeStat?.Reset();
            TerminalFirst = new SimQueue<Group>();

		    TerminalSecondLengthStat?.Reset();
		    TerminalSecondTimeStat?.Reset();
            TerminalSecond = new SimQueue<Group>();

		    RentalInLengthStat?.Reset();
            RentalInTimeStat?.Reset();
            RentalIn = new SimQueue<Group>();

		    RentalOutLengthStat?.Reset();
            RentalOutTimeStat?.Reset();
            RentalOut = new SimQueue<Group>();
        }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		private void Init()
		{
			new ManagerQueue(SimId.ManagerQueue, MySim, this);
			new ActionAddTerminalSecond(SimId.ActionAddTerminalSecond, MySim, this);
			new SchedulerGetOff(SimId.SchedulerGetOff, MySim, this);
			new ActionAddTerminalFirst(SimId.ActionAddTerminalFirst, MySim, this);
			new ActionAddRental(SimId.ActionAddRental, MySim, this);
			new SchedulerBoard(SimId.SchedulerBoard, MySim, this);
			AddOwnMessage(Mc.GetOffTerminalThird);
			AddOwnMessage(Mc.AddToRental);
			AddOwnMessage(Mc.BoardTerminalSecond);
			AddOwnMessage(Mc.BoardTerminalFirst);
			AddOwnMessage(Mc.GetNextCustomer);
			AddOwnMessage(Mc.AddToTerminalFirst);
			AddOwnMessage(Mc.AddToTerminalSecond);
			AddOwnMessage(Mc.HaveFreeService);
			AddOwnMessage(Mc.BoardRental);
			AddOwnMessage(Mc.ResetStats);
			AddOwnMessage(Mc.AddToRentalOut);
		}
		//meta! tag="end"
	}
}