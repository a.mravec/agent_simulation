using OSPABA;
using simulation;
using managers;
using continualAssistants;
using EventSimulation.Simulation.Statistics;
using instantAssistants;
namespace agents
{
	//meta! id="47"
	public class AgentRental : Agent
	{
        public int FreeNumberOfService { get; set; }

	    public Statistics ServiceStat { get; set; }

        public AgentRental(int id, Simulation mySim, Agent parent) :
			base(id, mySim, parent)
		{
			Init();
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
            // Setup component for the next replication
		    FreeNumberOfService = ((MySimulation) MySim).NumberOfService;

		    ServiceStat = new Statistics(MySim, false);
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		private void Init()
		{
			new ManagerRental(SimId.ManagerRental, MySim, this);
			new SchedulerService(SimId.SchedulerService, MySim, this);
			AddOwnMessage(Mc.GetNextCustomer);
			AddOwnMessage(Mc.HaveFreeService);
			AddOwnMessage(Mc.ResetStats);
			AddOwnMessage(Mc.UseService);
		}
		//meta! tag="end"

	    public void UpdateStat()
	    {
	        MySimulation mySimulation = (MySimulation) MySim;

            ServiceStat.Add(mySimulation.NumberOfService - FreeNumberOfService);
	        mySimulation.CallUpdateServiceStatistics();
        }
	}
}