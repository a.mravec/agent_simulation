using System;
using System.Collections.Generic;
using System.Configuration;
using OSPABA;
using simulation;
using managers;
using continualAssistants;
using Core.entities;
using instantAssistants;
namespace agents
{
	//meta! id="63"
	public class AgentBus : Agent
	{
        public int NumberOfBus { get; set; }
	    public int BusInStation { get; set; }
        public List<Bus> Busses { get; set; }

		public AgentBus(int id, Simulation mySim, Agent parent) :
			base(id, mySim, parent)
		{
			Init();
        }

		override public void PrepareReplication()
		{
			base.PrepareReplication();
            // Setup component for the next replication
		    if (((MySimulation) MySim).BusType == null)
		    {
                throw new Exception("Bus type is not configured.");
		    }
		    Busses = new List<Bus>(((MySimulation)MySim).NumberOfBus);
		    NumberOfBus = ((MySimulation) MySim).NumberOfBus;
		    BusInStation = 0;
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		private void Init()
		{
			new ManagerBus(SimId.ManagerBus, MySim, this);
			new SchedulerMoveBus(SimId.SchedulerMoveBus, MySim, this);
			AddOwnMessage(Mc.GetOffTerminalThird);
			AddOwnMessage(Mc.Init);
			AddOwnMessage(Mc.BoardTerminalSecond);
			AddOwnMessage(Mc.BoardTerminalFirst);
			AddOwnMessage(Mc.BoardRental);
			AddOwnMessage(Mc.ResetStats);
		}
		//meta! tag="end"
	}
}