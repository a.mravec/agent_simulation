using System.Security.Cryptography.X509Certificates;
using OSPABA;
using simulation;
using managers;
using continualAssistants;
using EventSimulation.Simulation.Statistics;
using instantAssistants;
namespace agents
{
	//meta! id="2"
	public class AgentOkolie : Agent
	{
	    public Statistics TimeInSystemLendCar { get; set; }
	    public Statistics TimeInSystemReturnCar { get; set; }

        public int Customers { get; set; }
        public int EndCustomer { get; set; }

	    public int TerminalFirst { get; set; }
	    public int TerminalSecond { get; set; }
	    public int Rental { get; set; }

        public AgentOkolie(int id, Simulation mySim, Agent parent) :
			base(id, mySim, parent)
		{
			Init();
		    TimeInSystemLendCar = new Statistics(MySim, true);
		    TimeInSystemReturnCar = new Statistics(MySim, true);
        }

		override public void PrepareReplication()
		{
			base.PrepareReplication();
            // Setup component for the next replication
		    TimeInSystemLendCar?.Reset();
		    TimeInSystemReturnCar?.Reset();
		    EndCustomer = 0;
		    Customers = 0;
		    TerminalFirst = 0;
		    TerminalSecond = 0;
		    Rental = 0;
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		private void Init()
		{
			new ManagerOkolie(SimId.ManagerOkolie, MySim, this);
			new ActionGenerateGroup(SimId.ActionGenerateGroup, MySim, this);
			new SchedulerArrivalTerminalSecond(SimId.SchedulerArrivalTerminalSecond, MySim, this);
			new SchedulerArrivalTerminalFirst(SimId.SchedulerArrivalTerminalFirst, MySim, this);
			new SchedulerArrivalRental(SimId.SchedulerArrivalRental, MySim, this);
			AddOwnMessage(Mc.Init);
			AddOwnMessage(Mc.EndCustomer);
			AddOwnMessage(Mc.ResetStats);
		}
		//meta! tag="end"
    }
}