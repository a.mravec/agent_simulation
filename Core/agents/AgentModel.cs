using OSPABA;
using simulation;
using managers;
using continualAssistants;
using instantAssistants;
namespace agents
{
	//meta! id="1"
	public class AgentModel : Agent
	{
		public AgentModel(int id, Simulation mySim, Agent parent) :
			base(id, mySim, parent)
		{
			Init();
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication
            MyMessage messageOkolie = new MyMessage(MySim);
		    messageOkolie.Code = Mc.Init;
		    messageOkolie.AddresseeId = SimId.AgentOkolie;
            MyManager.Notice(messageOkolie);

		    MyMessage messageBus = new MyMessage(MySim);
		    messageBus.Code = Mc.Init;
		    messageBus.AddresseeId = SimId.AgentBus;
		    MyManager.Notice(messageBus);

		    MyMessage myMessage = new MyMessage(MySim);
		    myMessage.Addressee = FindAssistant(SimId.SchedulerWarmUp); ;
		    myMessage.Code = Mc.Start;
		    MyManager.StartContinualAssistant(myMessage);
        }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		private void Init()
		{
			new ManagerModel(SimId.ManagerModel, MySim, this);
			new SchedulerWarmUp(SimId.SchedulerWarmUp, MySim, this);
		}
		//meta! tag="end"
	}
}