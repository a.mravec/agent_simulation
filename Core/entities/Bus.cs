﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Simulation.Statistics;
using OSPABA;
using OSPDataStruct;
using OSPStat;
using simulation;

namespace Core.entities
{
    public class Bus
    {
        public static int Counter { get; set; }

        public Statistics BusStat { get; set; }
        public SimQueue<Group> BusQueue { get; set; }

        public Routes Route { get; set; }
        public BusType Type { get; set; }
        public int OccupiedPlaces { get; set; } = 0;
        public double Distance { get; set; } = 0.0;
        public int Id { get; set; }

        public Simulation MySim { get; set; }

        public Bus(Simulation sim)
        {
            Id = ++Counter;
            Route = Routes.RentalToFirst;
            MySim = sim;
            BusStat = new Statistics(MySim, false);
            BusQueue = new SimQueue<Group>();
        }

        public Group Dequeue()
        {
            BusStat.Add(OccupiedPlaces);
            ((MySimulation)MySim).CallUpdateBusStatistics();
            ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | BUS (" + Id + ") | Bus OUT: " + (BusQueue.Count - 1) + " | Type Group: " + BusQueue.Peek().LendCar + " | Route: " + Route);
            OccupiedPlaces -= BusQueue.Peek().NumberCustomer;
            if (OccupiedPlaces < 0)
            {
                throw new Exception("Bad");
            }

            return BusQueue.Dequeue();
        }

        public void Enqueue(Group group)
        {
            BusStat.Add(OccupiedPlaces - group.NumberCustomer);
            ((MySimulation)MySim).CallUpdateBusStatistics();
            if (OccupiedPlaces > Type.Capacity)
            {
                throw new Exception("Bad");
            }

            BusQueue.Enqueue(group);
            ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | BUS (" + Id + ") | Bus IN: " + BusQueue.Count + " | Type Group: " + group.LendCar + " | Route: " + Route);
        }
    }
}
