﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using agents;
using OSPABA;
using simulation;

namespace Core.entities
{
    public class Group
    {
        public int Id { get; set; }
        public bool LendCar { get; set; }
        public double ArrivalTime { get; set; }
        public double StartTimeInRentalInQueue { get; set; }
        public double StartTimeInRentalOutQueue { get; set; }
        public int NumberCustomer { get; set; }

        public Simulation MySim { get; set; }

        public string StartTimeNice
        {
            get
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(ArrivalTime);
                return timeSpan.ToString(@"hh\:mm\:ss");
            }
        }

        public string LendCarNice
        {
            get
            {
                return (LendCar) ? "Yes" : "No";
            }
        }

        public Group(Simulation sim)
        {
            Id = ++((MySimulation)sim).AgentOkolie.Customers;
            MySim = sim;
        }

        public override string ToString()
        {
            return Id + ". Customer: " + NumberCustomer + ", Lend: " + LendCarNice;
        }
    }
}
