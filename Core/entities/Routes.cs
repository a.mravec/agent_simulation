﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.entities
{
    public enum Routes
    {
        RentalToFirst,
        FirstToSecond,
        SecondToRental,
        RentalToThird,
        ThirdToFirst
    }

    public class BusType
    {
        public string Type { get; set; }
        public int Capacity { get; set; }
        public double PricePerKm { get; set; }
    }
}
