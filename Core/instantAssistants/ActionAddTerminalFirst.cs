using OSPABA;
using simulation;
using agents;
using Core.entities;

namespace instantAssistants
{
	//meta! id="35"
	public class ActionAddTerminalFirst : Action
	{
		public ActionAddTerminalFirst(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
		}

		override public void Execute(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    MyAgent.TerminalFirstEnqueue(myMessage.Group);
            ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | TERMINAL | Terminal First: " + MyAgent.TerminalFirst.Count);
        }

		public new AgentQueue MyAgent
		{
			get
			{
				return (AgentQueue)base.MyAgent;
			}
		}
	}
}