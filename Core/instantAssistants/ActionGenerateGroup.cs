using System;
using OSPABA;
using simulation;
using agents;
using Core.entities;
using Action = OSPABA.Action;

namespace instantAssistants
{
	//meta! id="13"
	public class ActionGenerateGroup : Action
	{
        private Random RandomSizeGroup { get; }
		public ActionGenerateGroup(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
		    RandomSizeGroup = new Random();
        }

		override public void Execute(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage) message;

            Group newGroup = new Group(MySim) {ArrivalTime = MyAgent.MySim.CurrentTime};
		    double rand = RandomSizeGroup.NextDouble();
		    if (rand < 0.60)
		    {
		        newGroup.NumberCustomer = 1;
		    }
		    else if (rand < 0.80)
		    {
		        newGroup.NumberCustomer = 2;
            }
		    else if (rand < 0.95)
		    {
		        newGroup.NumberCustomer = 3;
		    }
		    else
		    {
		        newGroup.NumberCustomer = 4;
            }

		    myMessage.Group = newGroup;
		}

		public new AgentOkolie MyAgent
		{
			get
			{
				return (AgentOkolie)base.MyAgent;
			}
		}
	}
}