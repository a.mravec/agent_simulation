using OSPABA;
using simulation;
using agents;
using Core.entities;

namespace instantAssistants
{
	//meta! id="37"
	public class ActionAddTerminalSecond : Action
	{
		public ActionAddTerminalSecond(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
		}

		override public void Execute(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage) message;
            MyAgent.TerminalSecondEnqueue(myMessage.Group);
            ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | TERMINAL | Terminal Second: " + MyAgent.TerminalSecond.Count);
        }

		public new AgentQueue MyAgent
		{
			get
			{
				return (AgentQueue)base.MyAgent;
			}
		}
	}
}