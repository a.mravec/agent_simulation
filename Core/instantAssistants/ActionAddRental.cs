using OSPABA;
using simulation;
using agents;
namespace instantAssistants
{
	//meta! id="44"
	public class ActionAddRental : Action
	{
		public ActionAddRental(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
		}

		override public void Execute(MessageForm message)
		{
		    MyMessage myMessage = (MyMessage)message;
		    MyAgent.RentalInEnqueue(myMessage.Group);
            ((MySimulation)MySim).CallTestHandler("Time: " + MySim.CurrentTime + " | TERMINAL | Rental IN: " + MyAgent.RentalIn.Count);
        }

		public new AgentQueue MyAgent
		{
			get
			{
				return (AgentQueue)base.MyAgent;
			}
		}
	}
}