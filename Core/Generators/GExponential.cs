﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;
using simulation;

namespace EventSimulation.Generators
{
    public class GExponential : AGenerator
    {
        public MySimulation MySim { get; set; }
        public int[] Medians { get; set; }

        public GExponential(int[] medians, MySimulation mySim) : base((new Random()).Next())
        {
            MySim = mySim;
            Medians = medians;
        }

        public GExponential(int[] medians, int seed, MySimulation mySim) : base(seed)
        {
            MySim = mySim;
            Medians = medians;
        }

        private double GetMedian()
        {
            int index = 0;
            if (MySim.CurrentTime >= MySim.WarmUpTime)
            {
                index = (int)Math.Floor(((MySim.CurrentTime - MySim.WarmUpTime) / 60) / 15);
                index = index % Medians.Length;
                return 3600.0 / Medians[index];
            }
            else
            {
                index = (int)Math.Floor((MySim.CurrentTime / 60) / 15);
                index = index % Medians.Length;
                return 3600.0 / Medians[0];
            }
        }

        private double GetTime()
        {
            if (MySim.CurrentTime >= MySim.WarmUpTime)
            {
                return MySim.CurrentTime - MySim.WarmUpTime;
            }
            else
            {
                return MySim.CurrentTime;
            }
        }

        public override double GetNumber()
        {
            double number =  (- Math.Log(1.0 - Rand.NextDouble(), Math.E)) / (1.0 / GetMedian());
            double t = GetTime();
            int i = (int)Math.Floor(t / 900);

            while (true)
            {
                double ti = (i + 1) * 900;
                if (t + number < ti)
                {
                    break;
                }

                if (i == Medians.Length - 1)
                {
                    break;
                }

                number = (number - (ti - t)) * (Medians[i] / (double)Medians[i + 1]);
                t = ti;
                i++;
            }

            return t + number - GetTime();
        }
    }
}
