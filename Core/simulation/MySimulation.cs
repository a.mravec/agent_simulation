using System;
using System.Linq;
using OSPABA;
using agents;
using Core.entities;
using EventSimulation.Simulation.Statistics;
using OSPStat;

namespace simulation
{
	public class MySimulation : Simulation
    {
        public int Id { get; set; }
        public double EndSimulation { get; set; } = 4.5 * 60 * 60;
        public double WarmUpTime { get; set; } = 2 * 60 * 60;
        public int NumberOfService { get; set; }
        public int NumberOfBus { get; set; }
        public BusType BusType { get; set; }

        public Statistics BusReplicationAverageStat { get; set; }
        public Statistics BusReplicationMaxStat { get; set; }

        public Statistics TimeInSystemLend { get; set; }
        public Statistics TimeInSystemReturn { get; set; }

        public double MonthMoney { get; set; }
        public double SimulationMoney { get; set; }
        public Statistics MonthMoneyStat { get; set; }
        public Statistics SimulationMoneyStat { get; set; }

        public MySimulation()
		{
			Init();
		    InitEvents();
        }

	    public void SetBusType(string type)
	    {
	        switch (type)
	        {
                case "A":
                    BusType = new BusType() {Type = "A", Capacity = 12, PricePerKm = 0.28};
                    break;

	            case "B":
	                BusType = new BusType() { Type = "B", Capacity = 18, PricePerKm = 0.43 };
                    break;

	            case "C":
	                BusType = new BusType() { Type = "C", Capacity = 30, PricePerKm = 0.54 };
                    break;

                default:
                    throw new NotImplementedException("This bus type is not implemented.");
                    break;
            }
	    }

	    override protected void PrepareSimulation()
	    {
	        base.PrepareSimulation();
	        EndSimulation += WarmUpTime;

            TimeInSystemLend = new Statistics(this, true);
	        TimeInSystemReturn = new Statistics(this, true);

	        MonthMoneyStat = new Statistics(this, true);
	        SimulationMoneyStat = new Statistics(this, true);
        }

	    override protected void PrepareReplication()
		{
			base.PrepareReplication();
            // Reset entities, queues, local statistics, etc...
		    Bus.Counter = 0;

		    BusReplicationAverageStat = new Statistics(this, true);
		    BusReplicationMaxStat = new Statistics(this, true);;
        }

		override protected void ReplicationFinished()
		{
		    // Collect local statistics into global, update UI, etc...
			base.ReplicationFinished();
		    TimeInSystemLend.Add(AgentOkolie.TimeInSystemLendCar.GetAverage());
		    TimeInSystemReturn.Add(AgentOkolie.TimeInSystemReturnCar.GetAverage());

            //Money();
		    MonthMoneyStat.Add(MonthMoney);
		    SimulationMoneyStat.Add(SimulationMoney);

            CallUpdateResults();
		}

		override protected void SimulationFinished()
		{
		    // Dysplay simulation results
			base.SimulationFinished();
        }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		private void Init()
		{
			AgentModel = new AgentModel(SimId.AgentModel, this, null);
			AgentOkolie = new AgentOkolie(SimId.AgentOkolie, this, AgentModel);
			AgentRental = new AgentRental(SimId.AgentRental, this, AgentModel);
			AgentQueue = new AgentQueue(SimId.AgentQueue, this, AgentModel);
			AgentBus = new AgentBus(SimId.AgentBus, this, AgentModel);
		}
		public AgentModel AgentModel
		{ get; set; }
		public AgentOkolie AgentOkolie
		{ get; set; }
		public AgentRental AgentRental
		{ get; set; }
		public AgentQueue AgentQueue
		{ get; set; }
		public AgentBus AgentBus
		{ get; set; }
		//meta! tag="end"

        // events
        public delegate void TestHandler(string s);
        public event TestHandler TestEvent;

        public delegate void SetCurrentTimeHandler(double time);
        public event SetCurrentTimeHandler SetCurrentTimeEvent;

        public delegate void UpdateGroupToTerminalFirstQueueHandler(Group group, bool add, Statistics statLength, Statistics statTime);
        public event UpdateGroupToTerminalFirstQueueHandler UpdateGroupToTerminalFirstQueueEvent;

        public delegate void UpdateGroupToTerminalSecondQueueHandler(Group group, bool add, Statistics statLength, Statistics statTime);
        public event UpdateGroupToTerminalSecondQueueHandler UpdateGroupToTerminalSecondQueueEvent;

        public delegate void UpdateGroupToRentalInQueueHandler(Group group, bool add, Statistics statLength, Statistics statTime);
        public event UpdateGroupToRentalInQueueHandler UpdateGroupToRentalInQueueEvent;

        public delegate void UpdateGroupToRentalOutQueueHandler(Group group, bool add, Statistics statLength, Statistics statTime);
        public event UpdateGroupToRentalOutQueueHandler UpdateGroupToRentalOutQueueEvent;

        public delegate void UpdateBusStatisticsHandler(Statistics statAverage, Statistics statMax);
        public event UpdateBusStatisticsHandler UpdateBusStatistics;

        public delegate void UpdateServiceStatisticsHandler(Statistics stat);
        public event UpdateServiceStatisticsHandler UpdateServiceStatistics;

        public delegate void UpdateMoneyHandler(double month, double simulation);
        public event UpdateMoneyHandler UpdateMoney;

        public delegate void UpdateTimeInSystemHandler(Statistics lendCar, Statistics returnCar, double time);
        public event UpdateTimeInSystemHandler UpdateTimeInSystemEvent;

        public delegate void UpdateResultsHandler(MySimulation s);
        public event UpdateResultsHandler UpdateResults;

        public void Money()
        {
            double time = CurrentTime - WarmUpTime;
            if (time > 0)
            {
                SimulationMoney = (NumberOfBus * 12.5) * (time / 3600);
                SimulationMoney += (NumberOfService * 11.5) * (time / 3600);
                SimulationMoney += AgentBus.Busses.Sum(x => x.Distance) * BusType.PricePerKm;

                MonthMoney = (SimulationMoney / time) * 60 * 60 * 24 * 30;
            }
        }

        public void Basic()
        {
            SetCurrentTimeEvent?.Invoke(CurrentTime);
            Money();
            UpdateMoney?.Invoke(MonthMoney, SimulationMoney);
            if (CurrentTime > EndSimulation && AgentOkolie.Customers <= AgentOkolie.EndCustomer && AgentBus.BusInStation == AgentBus.NumberOfBus)
            {
                StopReplication();
            }
        }

        public void InitEvents()
        {
            OnRefreshUI(simulation => { Basic(); });
        }

        public void CallTestHandler(string text)
        {
            TestEvent?.Invoke(text);
        }

        public void CallUpdateGroupToTerminalFirst(Group group, bool add)
        {
            
            UpdateGroupToTerminalFirstQueueEvent?.Invoke(group, add, AgentQueue.TerminalFirstLengthStat,
                AgentQueue.TerminalFirstTimeStat);
            
        }

        public void CallUpdateGroupToTerminalSecond(Group group, bool add)
        {
            
            UpdateGroupToTerminalSecondQueueEvent?.Invoke(group, add, AgentQueue.TerminalSecondLengthStat,
                AgentQueue.TerminalSecondTimeStat);
            
        }

        public void CallUpdateGroupToRentalIn(Group group, bool add)
        {
            
            UpdateGroupToRentalInQueueEvent?.Invoke(group, add, AgentQueue.RentalInLengthStat,
                AgentQueue.RentalInTimeStat);
            
        }

        public void CallUpdateGroupToRentalOut(Group group, bool add)
        {
            
            UpdateGroupToRentalOutQueueEvent?.Invoke(group, add, AgentQueue.RentalOutLengthStat,
                AgentQueue.RentalOutTimeStat);
            
        }

        public void CallUpdateTimeInSystem()
        {
            if (CurrentTime > WarmUpTime)
            {
                UpdateTimeInSystemEvent?.Invoke(AgentOkolie.TimeInSystemLendCar, AgentOkolie.TimeInSystemReturnCar,
                    CurrentTime);
            }
        }

        public void CallUpdateBusStatistics()
        {
            
            foreach (Bus bus in AgentBus.Busses)
            {
                BusReplicationAverageStat.Add(bus.BusStat.GetAverage());
                BusReplicationMaxStat.Add(bus.BusStat.MaxValue);
            }

            UpdateBusStatistics?.Invoke(BusReplicationAverageStat, BusReplicationMaxStat);
            
        }

        public void CallUpdateServiceStatistics()
        {

            UpdateServiceStatistics?.Invoke(AgentRental.ServiceStat);
            
        }

        public void CallUpdateResults()
        {
            if (CurrentTime >= WarmUpTime)
            {
                UpdateResults?.Invoke(this);
            }
        }
    }
}