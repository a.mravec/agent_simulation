using Core.entities;
using OSPABA;
namespace simulation
{
	public class MyMessage : MessageForm
	{
        public Bus Bus { get; set; }
        public Group Group { get; set; }
        public bool FreeService { get; set; }

		public MyMessage(Simulation sim) :
			base(sim)
		{
		}

		public MyMessage(MyMessage original) :
			base(original)
		{
			// copy() is called in superclass
		}

		override public MessageForm CreateCopy()
		{
			return new MyMessage(this);
		}

		override protected void Copy(MessageForm message)
		{
			base.Copy(message);
			MyMessage original = (MyMessage)message;
			// Copy attributes
		    Group = original.Group;
		    FreeService = original.FreeService;
		    Bus = original.Bus;
		}
	}
}