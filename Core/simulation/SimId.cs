using OSPABA;
namespace simulation
{
	public class SimId : IdList
	{
		//meta! userInfo="Generated code: do not modify", tag="begin"
		public const int AgentModel = 1;
		public const int AgentOkolie = 2;
		public const int AgentRental = 6;
		public const int AgentQueue = 4;
		public const int AgentBus = 7;
		public const int ManagerModel = 101;
		public const int ManagerOkolie = 102;
		public const int ManagerRental = 106;
		public const int ManagerQueue = 104;
		public const int ManagerBus = 107;
		public const int ActionAddTerminalFirst = 1005;
		public const int SchedulerMoveBus = 1009;
		public const int ActionAddTerminalSecond = 1006;
		public const int SchedulerArrivalTerminalFirst = 1001;
		public const int SchedulerArrivalTerminalSecond = 1002;
		public const int SchedulerArrivalRental = 1003;
		public const int ActionAddRental = 1007;
		public const int ActionGenerateGroup = 1004;
		public const int SchedulerBoard = 1010;
		public const int SchedulerGetOff = 1011;
		public const int SchedulerService = 1008;
		public const int SchedulerWarmUp = 1012;
		//meta! tag="end"
	}
}