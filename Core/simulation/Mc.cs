using OSPABA;
namespace simulation
{
	public class Mc : IdList
	{
		//meta! userInfo="Generated code: do not modify", tag="begin"
		public const int Init = 1001;
		public const int BoardTerminalSecond = 1012;
		public const int BoardRental = 1013;
		public const int BoardTerminalFirst = 1014;
		public const int GetOffTerminalThird = 1016;
		public const int EndCustomer = 1017;
		public const int ResetStats = 1018;
		public const int AddToTerminalFirst = 1002;
		public const int AddToTerminalSecond = 1003;
		public const int AddToRental = 1004;
		public const int HaveFreeService = 1005;
		public const int UseService = 1007;
		public const int AddToRentalOut = 1008;
		public const int GetNextCustomer = 1009;
		//meta! tag="end"

		// 1..1000 range reserved for user
	}
}