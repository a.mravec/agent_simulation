﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation.Simulation.Statistics
{
    public class Average
    {
        public double CurrentAverage { get; set; }
        public int Count { get; set; }

        public void Add(double value)
        {
            CurrentAverage = (CurrentAverage * Count + value) / (Count + 1);
            Count++;
        }

        public void Reset()
        {
            CurrentAverage = 0;
            Count = 0;
        }
    }
}
