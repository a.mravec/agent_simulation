﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation.Simulation.Statistics
{
    public class WeightAverage
    {
        public double CurrentAverage { get; set; } = 0;
        public double TotalWeight { get; set; } = 0;
        public double LastTime { get; set; } = 0;

        public void Add(double value, double currentTime)
        {
            double timeDifference = currentTime - LastTime;
            LastTime = currentTime;
            if (TotalWeight + timeDifference > 0)
            {
                CurrentAverage = (CurrentAverage * TotalWeight + value * timeDifference) / (TotalWeight + timeDifference);
            }
            TotalWeight += timeDifference;
        }

        public void Reset()
        {
            CurrentAverage = 0;
            TotalWeight = 0;
            LastTime = 0;
        }
    }
}
